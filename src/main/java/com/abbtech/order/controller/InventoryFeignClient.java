package com.abbtech.order.controller;


import java.util.UUID;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "inventoryFeignClient",url = "http://localhost:8087/abbtech-bookstore-inventory-ms")
public interface InventoryFeignClient {


    @GetMapping("/inventorycheck")
    boolean checkBook(@RequestParam("bookId") UUID bookId);

}
