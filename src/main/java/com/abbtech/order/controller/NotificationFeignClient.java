package com.abbtech.order.controller;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


@FeignClient(name = "notificationFeignClient",url = "http://localhost:8086/abbtech-bookstore-notification-ms")
public interface NotificationFeignClient {

    @GetMapping("/")
    String sendNotification(@RequestParam("orderId") String orderId);


}
