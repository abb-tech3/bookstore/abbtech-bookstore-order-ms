package com.abbtech.order.controller;

import java.util.UUID;
import com.abbtech.order.dto.request.CreateOrderRequestDto;
import com.abbtech.order.service.impl.OrderServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping()
@RequiredArgsConstructor
@Validated
public class OrderController {


    private final NotificationFeignClient notificationFeignClient;
    private final InventoryFeignClient inventoryFeignClient;
    private final OrderServiceImpl orderService;
    private final KafkaTemplate<String, Object> kafkaTemplate;




    @GetMapping("/confirm")
    public void confirmOrder(@RequestParam("orderId") UUID orderId){
        orderService.confirmOrder(orderId);
    }

    @GetMapping()
    public void test(){
        System.out.println("bla");
    }

    @PostMapping("/create")
    public String createOrder(@RequestBody CreateOrderRequestDto order){
        boolean bookExist=inventoryFeignClient.checkBook(order.getBookId());
        if (!bookExist){
            return "This book is not exist";
        }
        orderService.saveOrder(order);
        return "Order accepted";

    }

}
