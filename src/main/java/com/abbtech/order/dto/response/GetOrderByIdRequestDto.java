package com.abbtech.order.dto.response;

import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GetOrderByIdRequestDto {
    private UUID orderId;
    private UUID bookID;
    private Integer quantity;
    private UUID userId;
}
