package com.abbtech.order.repository;

import java.util.UUID;
import com.abbtech.order.entity.Orders;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Orders, Long> {

    Orders getOrderById(UUID id);
}
