package com.abbtech.order.service;

import java.util.UUID;
import com.abbtech.order.dto.request.CreateOrderRequestDto;
import com.abbtech.order.dto.response.GetOrderByIdRequestDto;

public interface OrderService {
    void saveOrder(CreateOrderRequestDto order);

    GetOrderByIdRequestDto getOrderById(UUID orderId);
}
