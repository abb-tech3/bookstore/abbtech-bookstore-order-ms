package com.abbtech.order.service.impl;

import java.util.UUID;
import com.abbtech.order.dto.NotificationDto;
import com.abbtech.order.dto.request.CreateOrderRequestDto;
import com.abbtech.order.dto.response.GetOrderByIdRequestDto;
import com.abbtech.order.entity.Orders;
import com.abbtech.order.repository.OrderRepository;
import com.abbtech.order.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final KafkaTemplate<String, Object> kafkaTemplate;
    private static final String TOPIC = "order_confirmed";

    @Override
    public void saveOrder(CreateOrderRequestDto order) {
        Orders orderEntity = Orders.builder()
                .id(UUID.randomUUID())
                .bookId(order.getBookId())
                .quantity(order.getQuantity())
                .userId(order.getUserId())
                .build();
        orderRepository.save(orderEntity);
    }

    @Override
    public GetOrderByIdRequestDto getOrderById(UUID orderId) {
        Orders order = orderRepository.getOrderById(orderId);
        return new GetOrderByIdRequestDto(order.getId(),order.getBookId(),order.getQuantity(),order.getUserId());
    }

    public void confirmOrder(UUID orderId) {
        Orders order = orderRepository.getOrderById(orderId);
        if (order == null){
            System.out.println("This order do not exist");
            return;
        }
        NotificationDto notificationDto = new NotificationDto(
                orderId, order.getUserId(),"order confirmed"
        );
        kafkaTemplate.send(TOPIC, notificationDto);
    }

}
